import React, { useEffect } from "react";
import "./Insights.css";
import HeaderComponent from "../HeaderComponent/HeaderComponent";
import leftImage from "../../assets/insights/Layer 6.png";
import insights from "../../assets/insights/insights.png";
import settingsIcon from "../../assets/insights/Layer 116 copy 6.png";
import FooterComponent from "../FooterComponent/FooterComponent";
import footerbg from "../../assets/insights/footerbg.png";
import { useState } from "react/cjs/react.development";

const Insights = ({ activePage, setActivePage }) => {
  useEffect(() => {
    setActivePage("insights");
    return () => {
      setActivePage();
    };
  }, []);
  return (
    <div style={{ overflowX: "hidden" }}>
      <div className="insights-bg" style={{ position: "relative" }}>
        <HeaderComponent activePage={activePage} />
        {/* main screen starts here */}
        <div className="row pt-4">
          <div className="col-lg-7 col-md-12 col-sm-12 my-xl-5 my-lg-4 my-md-5 my-sm-0">
            <div className="mt-5">
              {/* <div className="col-md-12 col-lg-8"></div> */}
              <div className="px-5 position-relative lg-my-0 sm-my-5">
                <img src={insights} alt="" className="rightImg mr-md-3" />
                <h2 className="text-white px-lg-5 px-sm-2  insightHeading">
                  The TestOptimize insights
                </h2>
              </div>
              <div className="insight-textbg px-5">
                <div
                  className="mx-lg-5 mx-sm-0 py-4"
                  style={{ textAlign: "left" }}
                >
                  <h5 className="col-12 insightSubHeading">
                    Insights and ideas to ponder on
                  </h5>
                  <p
                    className="col-12"
                    style={{
                      fontSize: "12px",
                      opacity: 0.5,
                      fontFamily: "Open Sans , sans-serif",
                      color: "#FFFFFF",
                    }}
                  >
                    Simple to adopt and elegantly designed, with the power to
                    give your real testing acceleration. Achieve sustainable
                    automation with no technical barriers. It’s amazing what AI
                    can do !
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-md-12 col-sm-12 my-xl-5 my-lg-4 my-md-5 my-sm-0">
            <div className="kindpng-bg">
              <img
                src={leftImage}
                alt=""
                className="leftImg mt-2 my-lg-0 my-sm-5"
              />
            </div>
          </div>
        </div>
      </div>
      {/* main screen ends here */}
      {/* second seperation starts here */}
      <div className="position-relative m-0">
        <div className="position-absolute centerLine"></div>
        <div className="lefttopbg">
          <div className="pt-5">
            <div className="container row w-100 mx-auto justify-content-between">
              <div className="col-lg-5 mx-auto px-md-5 px-sm-5 px-xs-5 px-lg-0">
                <div>
                  <h4 className="text-left pb-3">
                    TestOptimize element locator plugin
                  </h4>
                </div>
                <div className="row">
                  <div className="col-2">
                    <img className="settingsIcon" src={settingsIcon} />
                  </div>
                  <div className="col-8">
                    <p className="px-2">
                      With TestOptimize element locator plugin now you can
                      capture all your web elements with faster. Does all
                      hardwork for you to capture precised locator value. Also
                      provides solution to all your algorithms
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-5 px-md-5 px-sm-5 px-xs-5 px-lg-0">
                <div>
                  <h4 className="text-left pb-3">
                    Self-healing autonomic test automation
                  </h4>
                </div>
                <div className="row">
                  <div className="col-2">
                    <img className="settingsIcon" src={settingsIcon} />
                  </div>
                  <div className="col-8">
                    <p className="px-2">
                      TestOptimize analytic runtime engine ensures a reliable
                      test execution by intelligently adapting to unexpected
                      application changes. Design-first approach ensure Robust
                      element ID based on AI.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="rounded-pill px-lg-4 px-md-2 verticalBorder">
            <hr className="verticalLine"></hr>
          </div>

          <div className="pt-5">
            <div className="container row w-100 mx-auto justify-content-between">
              <div className="col-lg-5 mx-auto px-md-5 px-sm-5 px-xs-5 px-lg-0">
                <div>
                  <h4 className="text-left pb-3">
                    TestOptimize Centralized Repository
                  </h4>
                </div>
                <div className="row">
                  <div className="col-2">
                    <img className="settingsIcon" src={settingsIcon} />
                  </div>
                  <div className="col-8">
                    <p className="px-2">
                      With TestOptimize element locator plugin now you can
                      capture all your web elements with faster. Does all
                      hardwork for you to capture precised locator value. Also
                      provides solution to all your algorithms
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-5 px-md-5 px-sm-5 px-xs-5 px-lg-0">
                <dv>
                  <h4 className="text-left pb-3">
                    AI Based Optimizer to remove trash
                  </h4>
                </dv>
                <div className="row">
                  <div className="col-2">
                    <img className="settingsIcon" src={settingsIcon} />
                  </div>
                  <div className="col-8">
                    <p className="px-2">
                      With TestOptimize element locator plugin now you can
                      capture all your web elements with faster. Does all
                      hardwork for you to capture precised locator value. Also
                      provides solution to all your algorithms
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="rounded-pill px-lg-4 px-md-2 verticalBorder ">
            <hr className="verticalLine"></hr>
          </div>
          <div className="pt-5">
            <div className="container row w-100 mx-auto justify-content-between">
              <div className="col-lg-5 mx-auto px-md-5 px-sm-5 px-xs-5 px-lg-0">
                <div>
                  <h4 className="text-left pb-3">Steps Reusability</h4>
                </div>
                <div className="row">
                  <div className="col-2">
                    <img className="settingsIcon" src={settingsIcon} />
                  </div>
                  <div className="col-8">
                    <p className="px-2">
                      With TestOptimize element locaotr plugin now you can
                      capture all your web elements with faster. Does all
                      hardwork for you to capture precised locator value. Also
                      provides solution to all your algorithms
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-5 px-md-5 px-sm-5 px-xs-5 px-lg-0">
                <div>
                  <h4 className="text-left pb-3">Custom Functions</h4>
                </div>
                <div className="row">
                  <div className="col-2">
                    <img className="settingsIcon" src={settingsIcon} />
                  </div>
                  <div className="col-8">
                    <p className="px-2">
                      With TestOptimize element locaotr plugin now you can
                      capture all your web elements with faster. Does all
                      hardwork for you to capture precised locator value. Also
                      provides solution to all your algorithms
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="rounded-pill px-lg-4 px-md-2 verticalBorder">
            <hr className="verticalLine"></hr>
          </div>
        </div>
        <div className="rightbottombg pb-4">
          <div className="pt-5">
            <div className="container row w-100 mx-auto justify-content-between">
              <div className="col-lg-5 mx-auto px-md-5 px-sm-5 px-xs-5 px-lg-0">
                <div>
                  <h4 className="text-left pb-3">
                    API & UI automated in the same flow
                  </h4>
                </div>
                <div className="row">
                  <div className="col-2">
                    <img className="settingsIcon" src={settingsIcon} />
                  </div>
                  <div className="col-8">
                    <p className="px-2">
                      Unique capability to integrate API and UI testing in the
                      same flow, enabling true end- to-end validation without
                      handoffs. API testing at the same simplicity and
                      regression maturity as UI automation.
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-5 px-md-5 px-sm-5 px-xs-5 px-lg-0">
                <div>
                  <h4 className="text-left pb-3">
                    Web & Mobile in the single flow
                  </h4>
                </div>
                <div className="row">
                  <div className="col-2">
                    <img className="settingsIcon" src={settingsIcon} />
                  </div>
                  <div className="col-8">
                    <p className="px-2">
                      Unique capability to integrate Web and Mobile testing in
                      the same flow, enabling true end-to-end validation without
                      handoffs. API testing at the same simplicity and
                      regression maturity as UI automation.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="rounded-pill px-lg-4 px-md-2 verticalBorder">
            <hr className="verticalLine"></hr>
          </div>
          <div className="pt-5">
            <div className="container row w-100 mx-auto justify-content-between">
              <div className="col-lg-5 mx-auto px-md-5 px-sm-5 px-xs-5 px-lg-0">
                <div>
                  <h4 className="text-left pb-3">
                    Powerful NLP for all your needs
                  </h4>
                </div>
                <div className="row">
                  <div className="col-2">
                    <img className="settingsIcon" src={settingsIcon} />
                  </div>
                  <div className="col-8">
                    <p className="px-2">
                      Unique capability to integrate API and UI testing in the
                      same flow, enabling true end- to-end validation without
                      handoffs. API testing at the same simplicity and
                      regression maturity as UI automation.
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-5 px-md-5 px-sm-5 px-xs-5 px-lg-0">
                <div>
                  <h4 className="text-left pb-3">Seamless CI/CD integration</h4>
                </div>
                <div className="row">
                  <div className="col-2">
                    <img className="settingsIcon" src={settingsIcon} />
                  </div>
                  <div className="col-8">
                    <p className="px-2">
                      Native integration with CI/CD tools such as Jira, Jenkins
                      ensure test automation is integral to development
                      lifecycle. Traceability is redefined with the intelligent,
                      connected test repository.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="rounded-pill px-lg-4 px-md-2 verticalBorder">
            <hr className="verticalLine"></hr>
          </div>
          <div className="pt-5">
            <div className="container row w-100 mx-auto justify-content-between">
              <div className="col-lg-5 mx-auto px-md-5 px-sm-5 px-xs-5 px-lg-0">
                <div>
                  <h4 className="text-left pb-3">Cloud Machine Integration</h4>
                </div>
                <div className="row">
                  <div className="col-2">
                    <img className="settingsIcon" src={settingsIcon} />
                  </div>
                  <div className="col-8">
                    <p className="px-2">
                      Unique capability to integrate API and UI testing in the
                      same flow, enabling true end- to-end validation without
                      handoffs. API testing at the same simplicity and
                      regression maturity as UI automation.
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-5 px-md-5 px-sm-5 px-xs-5 px-lg-0">
                <div>
                  <h4 className="text-left pb-3">Cloud Machine Integration</h4>
                </div>
                <div className="row">
                  <div className="col-2">
                    <img className="settingsIcon" src={settingsIcon} />
                  </div>
                  <div className="col-8">
                    <p className="px-2">
                      Native integration with CI/CD tools such as Jira, Jenkins
                      ensure test automation is integral to development
                      lifecycle. Traceability is redefined with the intelligent,
                      connected test repository.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Second seperation ends here */}
      <div className="m-0">
        <FooterComponent background={footerbg} image={true} />
      </div>
    </div>
  );
};

export default Insights;
