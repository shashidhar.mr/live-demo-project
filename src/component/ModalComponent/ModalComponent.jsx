import React, { useState, useEffect } from "react";
import "react-responsive-modal/styles.css";
import { Modal } from "react-responsive-modal";
import Calendar from "react-calendar";
import spacetime from "spacetime";
import TimezoneSelect, { i18nTimezones } from "react-timezone-select";
import "react-calendar/dist/Calendar.css";
import modalLogo from "../../assets/modal/modalLogo.png";
import TESTOPTIMIZE_Logo from "../../assets/modal/TESTOPTIMIZE_Logo.png";
import modal_title2 from "../../assets/modal/modal_title2.png";
import alarmImg from "../../assets/modal/alarmImg.png";
import vedioImg from "../../assets/modal/vedioImg.png";
import globe from "../../assets/modal/globe_icon.svg";

import "./ModalComponent.css";
const styles = {
  fontFamily: "sans-serif",
  textAlign: "center",
};

function ModalComponent({
  demoModalOpen,
  setDemoModalOpen,
  setIsValidField,
  setCheckBoxValue,
}) {
  const [open, setOpenModal] = useState(false);
  const [value, OnChange] = useState(new Date());
  const [newtime, setNewTime] = useState("");
  const [mydate, setMyDate] = useState("");
  const [myday, setMyDay] = useState("");
  const [myMonth, setMyMonth] = useState("");
  const [tz, setTz] = useState(
    Intl.DateTimeFormat().resolvedOptions().timeZone
  );
  let [datetime, setDatetime] = useState(spacetime.now());
  let [timeHour, setTimeHour] = useState("");
  let [timeMin, setTimeMin] = useState("");

  let [period, setPeriod] = useState("");
  // let period = datetime.unixFmt("a");
  useEffect(() => {
    let timings = new Date();
    let date = timings.getDate();
    setMyDate(date);
    console.log(date);
    let weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    let day = weekday[timings.getDay()];
    setMyDay(day);
    console.log(day);

    let month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    let Totalmonth = month[timings.getMonth()];
    setMyMonth(Totalmonth);
    console.log(Totalmonth);
  }, []);

  useEffect(() => {
    debugger;
    console.log(demoModalOpen, "demovalue");
  }, []);

  useEffect(() => {
    console.log("dateTime", datetime);

    let timings = datetime.unixFmt("a");
    // let minutes = timings.getMinutes();
    setPeriod(timings);
  }, [datetime]);

  useEffect(() => {
    console.log("dateTime", datetime);

    let timings = datetime.unixFmt("mm");
    // let minutes = timings.getMinutes();
    setTimeMin(Number(timings));
  }, [datetime]);

  useEffect(() => {
    console.log("dateTime", datetime);
    let timehour = datetime.unixFmt("HH");
    // let hours = timings.getHours();

    setTimeHour(Number(timehour));
  }, [datetime]);

  const changeTime = () => {
    console.log("timeMin", timeMin);

    if (timeMin < 59) {
      let minUp = timeMin + 1;
      console.log(minUp);
      setTimeMin(minUp);
    }
  };

  useEffect(() => {
    console.log("8000", datetime.goto(tz.value));
    setDatetime(datetime.goto(tz.value));
  }, [tz]);

  const onOpenModal = () => {
    setDemoModalOpen(true);
  };

  const dateOnChange = (val) => {
    console.log("val", val);
    let date = val.getDate();
    setMyDate(date);
    console.log(date);
    let weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    let day = weekday[val.getDay()];
    setMyDay(day);
    console.log(day);

    let month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    let Totalmonth = month[val.getMonth()];
    setMyMonth(Totalmonth);
    console.log(Totalmonth);

    setNewTime(val);
  };

  const onCloseModal = () => {
    setDemoModalOpen(false);
  };
  const changeMin = () => {
    console.log("timeMin", timeMin);
    if (timeMin > 0) {
      let mindown = timeMin - 1;
      console.log(mindown);
      setTimeMin(mindown);
    }
  };
  const changeHour = () => {
    console.log("timeHour", timeHour);
    if (timeHour < 24) {
      if (timeHour < 11) {
        setPeriod("AM");
      } else {
        setPeriod("PM");
      }
      let hourup = timeHour + 1;
      console.log(hourup);
      setTimeHour(hourup);
    }
  };
  const changeHourDown = () => {
    console.log("timeHour", timeHour);
    if (timeHour > 1) {
      if (timeHour <= 12) {
        setPeriod("AM");
      } else {
        setPeriod("PM");
      }
      let hourdown = timeHour - 1;
      console.log(hourdown);
      setTimeHour(hourdown);
    }
  };
  const confirmData = () => {
    // setIsValidField(false);
    // setCheckBoxValue(false);
    setDemoModalOpen(false);
  };

  return (
    <div style={styles}>
      {demoModalOpen && (
        <Modal open={demoModalOpen} onClose={onCloseModal} className="modal1">
          <div className="container-fluid">
            <div className="row">
              <div className="col-lg-4 col-md-12 col-sm-12">
                <img src={modalLogo} className="modal_logo" alt="" />
                <img src={TESTOPTIMIZE_Logo} className="modal_title" alt="" />
                <div className="col-md-12 col-sm-12">
                  <img src={modal_title2} className="modal_title2" alt="" />
                </div>
                <div className=" col-md-12 col-sm-12">
                  <img
                    src={TESTOPTIMIZE_Logo}
                    className="modal_second_div"
                    alt=""
                  />
                  <h4 className="module_header4">
                    TestOptimize Partner meeting{" "}
                  </h4>
                </div>
                <div className=" col-md-12 col-sm-12">
                  <p className="modal_alarm_div">
                    <img src={alarmImg} className="modal_alarm" alt="" />{" "}
                    <span>45 min</span>
                  </p>
                  <p className="modal_alarm_div1">
                    <img src={vedioImg} className="modal_alarm" alt="" />{" "}
                    <span>
                      Web conference details provided upon confirmation
                    </span>{" "}
                  </p>
                </div>
                <div className=" col-md-12 col-sm-12">
                  <p className="modal_textData">
                    Thank you! for your interest in being TestOptimize partner.
                    We are excited to onboard you and get next steps planned.
                    Please select a date and time that works best for you and we
                    look forword to our discussion.
                  </p>
                </div>
              </div>
              <div className="col-lg-4 col-md-12 col-sm-12 second_modal">
                <div className="mb-lg-5 col-md-12 col-sm-12 modal_div_sec">
                  <h2 className="modal_select">Select a Date & Time</h2>
                </div>
                <div>
                  <Calendar
                    // onChange={(val) => {
                    //   console.log("val", val);
                    //   setNewTime(val);
                    // }}
                    onChange={dateOnChange}
                    value={value}
                  />
                </div>
              </div>
              <div className="col-lg-4 col-md-12 col-sm-12 mt-5">
                <div className="col-md-12 col-sm-12 modal_timeZone">
                  <h6 className="module_header6">Time Zone</h6>
                  <div className="container offset-lg-0 offset-md-1 offset-sm-1">
                    <div className="row">
                      <span
                        className="col-lg-2 col-md-1 col-sm-1"
                        style={{
                          paddingRight: "0px",
                          background: "#e6e6e6",
                          borderRadius: "10px 0px 0px 10px",
                        }}
                      >
                        {/* <i class="bi bi-globe" style={{fontSize:"24px",  marginLeft:"6px", marginTop:"6px"}}></i> */}
                        <img src={globe} className="globe" alt="" />
                      </span>
                      <span
                        className="col-lg-10 col-md-9 col-sm-10"
                        style={{ paddingLeft: "0px" }}
                      >
                        <TimezoneSelect
                          value={tz}
                          onChange={(val) => {
                            console.log("----", val);
                            setTz(val);
                          }}
                          timezones={{
                            ...i18nTimezones,
                            "America/Lima": "Pittsburgh",
                            "Europe/Berlin": "Frankfurt",
                          }}
                        />
                      </span>
                    </div>
                  </div>
                 
                  <div className="output-wrapper col-md-12 col-sm-12">
                    <div
                      className="module_third_div"
                      style={{ textAlign: "center", padding: "4%" }}
                    >
                      
                      <span
                        className="ampm"
                        style={{
                          color: period === "AM" ? "#0062cc" : "black",
                          padding: "5%",
                        }}
                      >
                        AM
                      </span>
                      <span>
                        |
                        <span
                          className="ampm"
                          style={{
                            color: period === "PM" ? "#0062cc" : "black",
                            padding: "5%",
                          }}
                        >
                          PM
                        </span>
                      </span>

                      <div className="">
                        <span
                          class=" arrowModalButton"
                          onClick={changeHour}
                          style={{ padding: "4%" }}
                        >
                          <i class="bi bi-chevron-up modal_iconstyle "></i>
                        </span>
                        {/* <i class="bi bi-chevron-up modal_iconstyle"></i> */}
                        <span
                          class="arrowModalButton"
                          onClick={changeTime}
                          style={{ padding: "4%" }}
                        >
                          <i class="bi bi-chevron-up modal_iconstyle"></i>
                        </span>
                      </div>
                      <div className="">
                        <span className="ampm"> {timeHour}</span>
                        <span
                          className="ampm"
                          style={{ paddingRight: "5%", paddingLeft: "4%" }}
                        >
                          {" "}
                          :{" "}
                        </span>{" "}
                        <span className="ampm"> {timeMin}</span>
                      </div>
                      <div className="mb-2">
                        <span
                          type="button"
                          class="arrowModalButton"
                          onClick={changeHourDown}
                          style={{ paddingRight: "7%" }}
                        >
                          <i class="bi bi-chevron-down modal_iconstyle"></i>
                        </span>

                        <span
                          type="button"
                          class=" arrowModalButton"
                          onClick={changeMin}
                          style={{ paddingLeft: "6%" }}
                        >
                          <i class="bi bi-chevron-down modal_iconstyle1"></i>
                        </span>
                      </div>
                      <div className="modal_daydiv">
                        <p className="ampm">
                          {myday} , {myMonth} {mydate}
                        </p>
                        {/* <p> {datetime.unixFmt("EEE , MMM dd ")}</p> */}
                      </div>
                      <div className="modal_daydiv ampm">
                        {/* <p> {datetime.unixFmt("HH:mm a")}</p> */}
                        <span className="ampm"> {timeHour}</span> :
                        <span className="ampm"> {timeMin}</span>{" "}
                        <span className="ampm">{period}</span>
                      </div>
                    </div>
                  </div>
                  <div className="modal_button">
                    <button
                      type="button"
                      class="modalbtn"
                      onClick={confirmData}
                    >
                      Confirm
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      )}
    </div>
  );
}

export default ModalComponent;
