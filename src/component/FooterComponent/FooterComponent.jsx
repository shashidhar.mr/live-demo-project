import React from "react";
import logo from "../../assets/logo.png";
import "./FooterComponent.css";
import { Link } from "react-router-dom";
import play from "../../assets/Footer/play.png";
import watchvideo from "../../assets/Footer/watchvideo.png";

const FooterComponent = ({ background, image }) => {
  return (
    <div>
      <div
        className="footerone"
        style={{ background: image ? `url(${background})` : `${background}` }}
      >
        <div className="container ">
          <div className="row align-items-center">
            <div className="col-lg-6 col-md-12 divcontent">
              <h6 className="footertitle">Ready to explore?</h6>
              <p className="footercontent">
                Let our Team walk you through how TestOptimize could help you to
                achieve Continuous testing
              </p>
            </div>
            <div className="col-lg-6 col-md-12 ">
              {" "}
              <div className="row">
                <div className=" w-50 mx-auto  h-100">
                  <button className="btn  reqdemobtn ">
                    <Link
                      className=""
                      to="/requestdemo"
                      style={{ textDecoration: "none" }}
                    >
                      <p className="m-0 text-white demorequestdemo requestdemotext">
                        {" "}
                        REQUEST A DEMO
                      </p>
                    </Link>
                  </button>
                </div>
                <div className="w-50 mx-auto h-100 ">
                  <img src={play} className="homePlayBtn d-inline" alt="" />
                  <h6 className="homePlayText d-inline text-white">
                    WATCH THE VIDEO
                  </h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container ">
        <div className="row">
          <div className="col-lg-2 col-md-4 col-sm-6">
            <ul>
              <li className="listtitle">Why TechOptimize</li>
              <li className="listdescription">Product Comparision</li>
              <li className="listdescription">Migrate to TestOptimize</li>
            </ul>
          </div>
          <div className="col-lg-2 col-md-4 col-sm-6">
            <ul>
              <li className="listtitle">Solutions</li>
              <li className="listdescription">Web Automation</li>
              <li className="listdescription">Mobile Automation</li>
              <li className="listdescription">API Integration</li>
              <li className="listdescription">Database Automation</li>
              <li className="listdescription">SAP Solutions</li>
            </ul>
          </div>
          <div className="col-lg-2 col-md-4 col-sm-6">
            <ul>
              <li className="listtitle">Insights</li>
              <li className="listdescription">Rich NLP Library</li>
              <li className="listdescription">Element Locator plugin</li>
            </ul>
          </div>
          <div className="col-lg-2 col-md-4 col-sm-6">
            <ul>
              <li className="listtitle">Pricing</li>
              <li className="listdescription">C-Basic</li>
              <li className="listdescription">C-Profesional</li>
              <li className="listdescription">E-On Primises</li>
            </ul>
          </div>

          <div className="col-lg-2 col-md-4 col-sm-6">
            <ul>
              <dt className="listtitle">About us</dt>
            </ul>
          </div>
          <div className="col-lg-2 col-md-4 col-sm-6">
            <ul>
              <dt className="listtitle">Company Address</dt>
            </ul>
          </div>
        </div>
      </div>
      <hr />
      {/* <div className="container-fluid">
        <span className="row">
               <span className="float-right "> 2020 TestOptimize Inc. All Rights Reserved</span> <span className="float-right "> Privacy policy</span> <span className="float-right "> Terms & Condition </span> 
        </span>

      </div> */}
      <div class="container pt-0 pb-4">
        <div class="copyright">
          &copy; Copyright 2020 TestOptimize Inc. All Rights Reserved
        </div>
        <div class="copyrightone ">Privacy policy</div>

        <div class="copyrightone ">Terms & Conditions</div>
      </div>
    </div>
  );
};

export default FooterComponent;
