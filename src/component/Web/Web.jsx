import React from "react";
import logo from "../../assets/logo.png";
import automated from "../../assets/automated-testing.png";
import firstdivweb from "../../assets/firstdivweb.png";
import firstdivlogo from "../../assets/firstdivlogo.png";
import divsecond from "../../assets/divsecond.png";
import windowsimg from "../../assets/windowsimg.png";
import chrome from "../../assets/chrome.png";
import mozilla from "../../assets/mozilla.png";
import opera from "../../assets/opera.png";
import edge from "../../assets/edge.png";
import compass from "../../assets/compass.png";
import book from "../../assets/book.png";
import computer from "../../assets/computer.png";
import teacher from "../../assets/teacher.png";
import footerbg from "../../assets/footerbg.png";
import custom from "../../assets/custom.png";
import reusability from "../../assets/reusability.png";
import backgroungimg from "../../assets/backgroungimg.png";

import "./Web.css";
import FooterComponent from "../FooterComponent/FooterComponent";
import HeaderComponent from "../HeaderComponent/HeaderComponent";

function Web({ activePage }) {
  return (
    <div style={{ overflowX: "hidden" }}>
      {/* ======= web_Hero Section ======= */}
      <section
        id="web_Hero"
        className="web_Hero align-items-center"
        style={{ background: `url(${backgroungimg})` }}
      >
        <div>
          <HeaderComponent activePage={activePage} />
        </div>
        <div className="container-fluid">
          <div
            className="row justify-content-lg-center web_firstrow web_thirddiv "
            style={{ marginTop: "5%" }}
          >
            <div
              className="col-lg-6 web_seconddiv"
              style={{ paddingRight: "10%", paddingLeft: "10%" }}
            >
              <div className="col-lg-12">
                <img src={firstdivlogo} className="web_firstdivlogo " alt="" />
              </div>
              <br />
              <div className="col-lg-12 ">
                <img src={firstdivweb} className="firstdivwebweb" alt="" />
              </div>
              <div className="col-lg-12">
                <h5 className="web_text-left web_firstdivtitle mb-4">
                  Develop your API test scripts with ease
                </h5>
                <p className="web_text-left web_firstdivcontent">
                  TestOptimize uses AI to create stable and reliable automated
                  tests faster than ever and to speed-up the executions and
                  maintenance of your automated tests. No coding skills
                  required.
                </p>
              </div>
            </div>
            <div className="col-lg-6 web_hero-img">
              <div className="container web_img-fluid">
                <img
                  src={automated}
                  style={{
                    maxWidth: "100%",
                    marginTop: "0%",
                    marginBottom: "0%",
                  }}
                />
              </div>
            </div>
          </div>
        </div>
        {/* <div className="container ">
          <div className="row web_firstrow web_thirddiv " style={{marginTop:"5%"}}>
            <div className="col-lg-6 web_seconddiv" style={{ paddingRight: "10%" }}>
              <div className="col-lg-12 ">
                <img src={firstdivlogo} className="web_firstdivlogo" alt="" />
              </div>
              <div className="col-lg-12  ">
                <img src={firstdivweb} className="firstdivweb" alt="" />
              </div>
              <div className="col-lg-12 ">
                <h5 className="web_text-left web_firstdivtitle mb-2">
                  Develop your web test scripts with ease
                </h5>
                <p className="web_text-left web_firstdivcontent">
                  TestOptimize uses AI to create stable and reliable automated
                  tests faster than ever and to speed-up the executions and
                  maintenance of your automated tests. No coding skills
                  required.
                </p>
              </div>
            </div>
            <div className="col-lg-6 web_hero-img ml-4 mt-lg-5 mt-md-1">
              <img src={automated} className="web_img-fluid" alt="" />
            </div>
          </div>
        </div> */}
      </section>
      <div className="container mx-auto col-lg-6 web_smallinbuilt ">
        <div className="p-2">Scriptless Web Automation</div>
      </div>
      {/* End we_Hero */}
      <div className="divsecond ">
        <div className="container-fluid  ">
          <div className="row mb-5">
            <div className="col-lg-6 col-md-12 col-sm-12">
              <img src={divsecond} className="divsecondimg" alt="" />
            </div>
            <div className="col-lg-6 col-md-12 mt-lg-4 col-sm-12">
              <ul className="ulListone">
                <li className="web_liList">Quick creation of web project</li>
                <li className="web_liList">
                  Capture your web elements using element locator plugin
                </li>
                <li className="web_liList">
                  Zero coding required to develop web test scripts
                </li>
                <li className="web_liList">Steps Reusability</li>

                <li className="web_liList">
                  Define your own NLp using custom functions
                </li>
                <li className="web_liList">
                  Run Your web scripts on your desired platform
                </li>
                <li className="web_liList">Create a Test suite</li>
                <li className="web_liList">
                  Run your test suite either sequentially or parallely
                </li>
              </ul>
            </div>
          </div>
          <div className="container mx-auto col-lg-6 web_smallinbuilt ">
            <div className="p-2">Supports Multiple platforms</div>
          </div>

          <div className="container ">
            <div className="row">
              <div className="col-lg-8 col-md-8 col-sm-12  offset-md-2 mt-0 mb-4 windowscontainer">
                <img src={windowsimg} className="windoesimg" alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="divbrowsers">
        <div className="container ">
          <div className="row">
            <div className="col-lg-6 col-md-6 col-sm-12 mt-5 mb-md-5">
              <h6 className="web_text-left web_firstdivtitle mb-2 mt-3">
                Execution
              </h6>
              <h3 className="web_text-left web_firstdivtitle mb-2 mt-4">
                Run your web test scripts
              </h3>

              <p className="web_text-left web_firstdivcontent mt-4">
                TestOptimize supports executing your web test scripts on
                multiple browsers
              </p>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12 mt-md-4 mb-sm-5">
              <div className="row">
                <div className="col-lg-4 col-md-4 col-sm-4 ">
                  <img src={chrome} className="browser mr-0" alt="" />
                </div>
                <div className="col-lg-4 col-md-4 col-sm-4 ">
                  <img src={mozilla} className="browser mr-0" alt="" />
                </div>
                <div className="col-lg-4 col-md-4 col-sm-4 ">
                  <img src={edge} className="browser mr-0" alt="" />
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4 col-md-4 col-sm-4 "></div>
                <div className="col-lg-4 col-md-4 col-sm-4 ">
                  <img src={opera} className="browser mr-0" alt="" />
                </div>
                <div className="col-lg-4 col-md-4 col-sm-4 ">
                  <img src={compass} className="browsercompose" alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* ======= small div ======= */}
      <div className="container col-lg-6 web_smallinbuilt ">
        <div className="p-2">Inbuilt AI based Test data driven framework</div>
      </div>

      <div className="web_divthird">
        <div className="container ">
          <div className="row ">
            <div className="col-lg-6 col-md-12 mt-lg-4 col-sm-12">
              <ul className="web_ulListtwo">
                <li className="web_liList">
                  Enhance your test coverage with in built data driven framework
                </li>
                <li className="web_liList">
                  Integrate test data to your scripts with simple logical
                  conditions
                </li>
                <li className="web_liList">
                  Enhance your test coverage with in built data driven framework
                </li>
                <li className="web_liList">
                  Integrate test data to your scripts with simple logical
                  conditions
                </li>
              </ul>
            </div>
            <div className="col-lg-6 col-md-12 col-sm-12 mb-5">
              <img src={book} className="divsecondimg" alt="" />
            </div>
          </div>
        </div>
      </div>

      <div className="container-fluid web_customfunction position-relative">
        <div
          className="position-absolute web_divider_line"
          style={{
            borderRight: "2px solid #ffffff",
            height: "90%",
            bottom: "0",
            width: "50%",
          }}
        ></div>

        <div className="row">
          <div className="col-lg-6 col-md-12 col-sm-12 mb-3">
            <div className="col-lg-12 ">
              <img src={reusability} className="web_customlogoone" alt="" />
            </div>
            <div className="col-lg-12 ">
              <h5 className="web_text-left web_customtitle mb-2">
                Steps Reusability
              </h5>
              <p className="web_text-left web_customcontent ">
                With AI driven now you can avoid writing repeated steps which
                come across in all your test cases
              </p>
            </div>
          </div>
          <div className="col-lg-6 col-md-12 col-sm-12">
            <div className="col-lg-12 ">
              <img src={custom} className="web_customlogoone" alt="" />
            </div>
            <div className="col-lg-12 ">
              <h5 className="web_text-left web_customtitle mb-2">
                Custom Functions
              </h5>
              <p className="web_text-left web_customcontent mb-5">
                Define your own NLp's which demands your test scenario
              </p>
            </div>
          </div>
        </div>
      </div>
      {/* ======= small div ======= */}
      <div className="container col-lg-6 web_smallinbuilt ">
        <div className="p-2">Test Execution Report</div>
      </div>
      <div className="web_testdiv">
        <div className="container ">
          <div className="row ">
            <div className="col-lg-6 col-md-12 col-sm-12">
              <img src={computer} className="web_computerimg" alt="" />
            </div>
            <div className="col-lg-6 col-md-12  col-sm-12 ">
              <ul className="web_ulListthree">
                <li className="web_liList ">
                  AI based report for all your test suite execution
                </li>
                <li className="web_liList">Execute only failed Test steps</li>
                <li className="web_liList">
                  Screen shots for all your failed test steps without any
                  congiguration
                </li>
                <li className="web_liList">
                  Send your test suite report for the selected group of people
                </li>
              </ul>
            </div>
          </div>
          <div className="container mx-auto col-lg-6 web_smallinbuilt mt-5 ">
            <div className="p-2">
              Test your application on multiple browsers
            </div>
          </div>
          <div className="container mb-3">
            <div className="row ">
              <div className="col-lg-6 col-md-12  col-sm-12 ">
                <ul className="web_ulListfour">
                  <li className="web_liList">Quick creation of web project</li>
                  <li className="web_liList">
                    With inbuilt cross browser testing feature you can test
                  </li>
                  <li className="web_liList">
                    Save your time on compatibility testing
                  </li>
                  <li className="web_liList">
                    Giving you better test coverage
                  </li>

                  <li className="web_liList">
                    With inbuilt cross browser testing feature you can test
                  </li>
                </ul>
              </div>
              <div className="col-lg-6 col-md-12 col-sm-12">
                <img src={teacher} className="web_teacherimg" alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <FooterComponent background={footerbg} image="true" />
    </div>
  );
}

export default Web;
