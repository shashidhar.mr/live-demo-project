import React, { useState, useEffect, useMemo } from "react";
import HeaderComponent from "../HeaderComponent/HeaderComponent";
// import BookDemo from "../../assets/Demo/BookDemo";
import bookDemo from "../../assets/Demo/BookDemo.png";
import "./Demo.css";
import footerbgdemo from "../../assets/Demo/footerbgdemo.png";
import ModalComponent from "../ModalComponent/ModalComponent";
import ProductImg from "../../assets/Demo/ProductImg.png";
import FooterComponent from "../FooterComponent/FooterComponent";
import "react-phone-number-input/style.css";
import PhoneInput, { isValidPhoneNumber } from "react-phone-number-input";
import { useForm } from "react-hook-form";
import validator from "validator";
import Select from "react-select";
import countryList from "react-select-country-list";

function Demo() {
  const [demoModalOpen, setDemoModalOpen] = useState(false);

  const options = useMemo(() => countryList().getData(), []);
  // values
  const [phone, setPhone] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [job, setJob] = useState("");
  const [role, setRole] = useState("");
  // const [phone, setPhone] = useState("");
  const [company, setCompany] = useState("");
  const [country, setCountry] = useState("");
  const [checkBoxValue, setCheckBoxValue] = useState(false);

  //Erros
  const [firstNameErr, setFirstNameErr] = useState({});
  const [lastNameErr, setLastNameErr] = useState({});
  const [emailErr, setEmailErr] = useState({});
  const [jobErr, setJobErr] = useState({});
  const [roleErr, setRoleErr] = useState({});
  const [phoneErr, setPhoneErr] = useState({});
  const [companyErr, setCompanyErr] = useState({});
  const [countryErr, setCountryErr] = useState({});
  const [isValidField, setIsValidField] = useState(false);

  const openModal = () => {};
  const onSubmit = (e) => {
    e.preventDefault();
    const isValid = formValidation();
    debugger;

    setIsValidField(isValid);
    if (isValid && checkBoxValue) {
      setDemoModalOpen(true);
      //send data to backend
      setFirstName("");
      setLastName("");
      setEmail("");
      setJob("");
      setRole("");
      setPhone("");
      setCompany("");
      setCountry("");
      // setIsValidField(false)
      setCheckBoxValue(false);
    }
  };

  useEffect(() => {
    setCheckBoxValue(false);
  }, []);

  const formValidation = () => {
    debugger;
    const firstNameErr = {};
    const lastNameErr = {};
    const emailErr = {};
    const jobErr = {};
    const roleErr = {};
    const phoneErr = {};
    const companyErr = {};
    const countryErr = {};

    let isValid = true;

    if (company.trim().length < 1) {
      companyErr.companyshort = "This field is required";
      isValid = false;
    }
    if (firstName.trim().length < 1) {
      firstNameErr.firstNameshort = "This field is required";
      isValid = false;
    }
    if (lastName.trim().length < 1) {
      lastNameErr.lastNameshort = "This field is required";
      isValid = false;
    }
    // if (email.trim().length < 1) {
    //   emailErr.emailshort = "This field is required";
    //   isValid = false;
    // }
    debugger;
    if (validator.isEmail(email)) {
      // setEmailError('Valid Email :)')
      emailErr.emailshort = "";
    } else {
      if (email.trim().length < 1) {
        emailErr.emailshort = "This field is required";
        isValid = false;
      } else {
        emailErr.emailshort = "Enter valid Email!";
        // setEmailError('Enter valid Email!')
        isValid = false;
      }
    }

    if (job.trim().length < 1) {
      jobErr.jobshort = "This field is required";
      isValid = false;
    }
    if (role.trim().length < 1) {
      roleErr.roleshort = "This field is required";
      isValid = false;
    }
    // if (phone.trim().length < 1) {
    phoneErr.phoneshort = phone
      ? isValidPhoneNumber(phone)
        ? undefined
        : "Invalid phone number"
      : "Phone number required";

    if (phoneErr.phoneshort) {
      isValid = false;
    }
    // }
    debugger;
    if (!country.value) {
      countryErr.countryshort = "This field is required";
      isValid = false;
    }

    setFirstNameErr(firstNameErr);
    setLastNameErr(lastNameErr);
    setEmailErr(emailErr);
    setJobErr(jobErr);
    setRoleErr(roleErr);
    setPhoneErr(phoneErr);
    setCountryErr(countryErr);
    setCompanyErr(companyErr);

    return isValid;
  };

  const changeCountry = (value) => {
    debugger;
    setCountry(value);
  };
  return (
    <div>
      <section id="maindiv" className="maindiv align-items-center">
        <div>
          <HeaderComponent />
        </div>
        <div className="container-fluid">
          <div
            className="row justify-content-lg-center  firstmain thirddiv1"
            style={{ marginTop: "5%" }}
          >
            <div className="col-lg-6 mainsecond">
              {/* <div className="col-lg-12 divmain ">
                <img src={bookDemo} className="div1img" alt="" />
              </div> */}
              <br />
              <div className="col-lg-12 mt-lg-5 divmain">
                <img src={bookDemo} className="divtitle" alt="" />
              </div>
              <div className="col-lg-12 divmain ">
                {/* <h5 className="text-left firstdivtitle1 mb-2">
                  Develop your API test scripts with ease
                </h5> */}
                <p className="text-left firstmaincontent1">
                  TestOptimize uses AI to create stable and reliable automated
                  tests faster than ever and to speed-up the executions and
                  maintenance of your automated tests. No coding skills
                  required.
                </p>
              </div>
            </div>
            <div className="col-lg-6 hero1-img">
              <div className="container mb-lg-5 mb-md-5">
                <img
                  src={ProductImg}
                  style={{
                    maxWidth: "97%",
                    marginTop: "2%",
                    marginBottom: "10%",
                  }}
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
      </section>
      <div className=" backgrounddiv">
        <div className="container">
          <div className="row ">
            <div className="col-lg-6 mb-lg-4 mt-lg-4 mt-md-4">
              <h6 className="contentheading">
                TestOptimize uses AI to create stable and reliable automated
                tests faster than ever and to speed-up the executions.
              </h6>
              <h5 className="contentheadingtwo">Your Demo Includes:</h5>
              <ul className="ulDemo">
                <li className="liDemo">
                  Scriptless AI & NLP based Test Automation solution
                </li>
                <li className="liDemo">
                  One Stop solution for all your Automation complexities
                </li>

                <li className="liDemo">
                  One Test script can handle data flow between multiple channels
                </li>

                <li className="liDemo">
                  Small POC on live applivcvations includes web, mobile API &
                  Database
                </li>

                <li className="liDemo">AP based element locator plugin</li>
                <li className="liDemo">AI based Report & Analytics</li>
                <li className="liDemo">
                  Cross platform & Cross Browser testing
                </li>
                <li className="liDemo">
                  Continous Integration / continous devilery integrations
                </li>
                <li className="liDemo">Custom plugin integration</li>
                <li className="liDemo">Root cause analysis</li>
              </ul>
            </div>
            <div className="col-lg-6">
              <div className="firstform mb-lg-4 mb-md-4 mb-sm-4 mb-xs-4">
                <h2 className="requestTitle" style={{ color: "blue" }}>
                  Request a Demo
                </h2>
                <div className="secondform">
                  <form onSubmit={onSubmit}>
                    <div className="form-group">
                      <label
                        for="exampleInputEmail1 "
                        className="labelTitleDemo"
                      >
                        First Name*
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="exampleInputEmail1"
                        aria-describedby="emailHelp"
                        value={firstName}
                        onChange={(e) => {
                          setFirstName(e.target.value);
                        }}
                        // {...register("name", {required:"Name is Required"})}
                      />
                      {Object.keys(firstNameErr).map((key) => {
                        debugger;
                        return (
                          <div style={{ color: "red" }}>
                            {firstNameErr[key]}
                          </div>
                        );
                      })}
                      {/* <small className="text-danger">This field is Required</small> */}
                    </div>
                    <div className="form-group">
                      <label
                        for="exampleInputPassword1"
                        className="labelTitleDemo"
                      >
                        Last Name*
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="exampleInputPassword1"
                        value={lastName}
                        onChange={(e) => {
                          setLastName(e.target.value);
                        }}
                        // {...register("lastname", {required:"LastName is Required"})}
                      />
                      {Object.keys(lastNameErr).map((key) => {
                        return (
                          <div style={{ color: "red" }}>{lastNameErr[key]}</div>
                        );
                      })}
                      {/* <small className="text-danger">This field is Required</small> */}
                    </div>
                    <div className="form-group">
                      <label
                        for="exampleInputPassword1"
                        className="labelTitleDemo"
                      >
                        Business Email Address*
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="exampleInputPassword1"
                        value={email}
                        onChange={(e) => {
                          setEmail(e.target.value);
                        }}
                        // {...register("email", {required:"email is Required"})}
                      />
                      {Object.keys(emailErr).map((key) => {
                        return (
                          <div style={{ color: "red" }}>{emailErr[key]}</div>
                        );
                      })}
                      {/* <small className="text-danger">This field is Required</small> */}
                    </div>
                    <div className="form-group">
                      <label
                        for="exampleFormControlSelect1"
                        className="labelTitleDemo"
                      >
                        Job Title*
                      </label>
                      <select
                        className="form-control"
                        id="exampleFormControlSelect1"
                        value={job}
                        onChange={(e) => {
                          setJob(e.target.value);
                        }}
                      >
                        <option></option>
                        <option>CEO</option>
                        <option>CTO</option>
                        <option>Project Manager</option>
                      </select>
                      {Object.keys(jobErr).map((key) => {
                        return (
                          <div style={{ color: "red" }}>{jobErr[key]}</div>
                        );
                      })}
                      {/* <small className="text-danger">This field is Required</small> */}
                    </div>
                    <div className="form-group">
                      <label
                        for="exampleInputPassword1"
                        className="labelTitleDemo"
                      >
                        Role*
                      </label>
                      <select
                        className="form-control"
                        id="exampleFormControlSelect1"
                        value={role}
                        onChange={(e) => {
                          setRole(e.target.value);
                        }}
                      >
                        <option></option>
                        <option>Developer</option>
                        <option>Tester</option>
                        <option>HR</option>
                      </select>
                      {Object.keys(roleErr).map((key) => {
                        return (
                          <div style={{ color: "red" }}>{roleErr[key]}</div>
                        );
                      })}
                      {/* <small className="text-danger">This field is Required</small> */}
                    </div>
                    <div className="form-group">
                      <label
                        for="exampleInputPassword1"
                        className="labelTitleDemo"
                      >
                        Phone Number*
                      </label>
                      <PhoneInput
                        // placeholder="Enter phone number"
                        value={phone}
                        onChange={setPhone}
                        className="phoneField"
                        // value={phone}
                        // onChange={(e)=>{setPhone(e.target.value)}}
                      />
                      {Object.keys(phoneErr).map((key) => {
                        return (
                          <div style={{ color: "red" }}>{phoneErr[key]}</div>
                        );
                      })}
                    </div>
                    <div className="form-group">
                      <label
                        for="exampleInputPassword1"
                        className="labelTitleDemo"
                      >
                        Company*
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="exampleInputPassword1"
                        value={company}
                        onChange={(e) => {
                          setCompany(e.target.value);
                        }}
                      />
                      {Object.keys(companyErr).map((key) => {
                        return (
                          <div style={{ color: "red" }}>{companyErr[key]}</div>
                        );
                      })}
                    </div>
                    {/* <div className="form-group">
                      <label
                        for="exampleInputPassword1"
                        className="labelTitleDemo"
                      >
                        Country*
                      </label>
                      <select
                        className="form-control"
                        id="exampleFormControlSelect1"
                        value={country}
                        onChange={(e) => {
                          setCountry(e.target.value);
                        }}
                      >
                        <option></option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>
                      {Object.keys(countryErr).map((key) => {
                        return (
                          <div style={{ color: "red" }}>{countryErr[key]}</div>
                        );
                      })}
                    </div> */}

                    <div className="form-group">
                      <label
                        for="exampleInputPassword1"
                        className="labelTitleDemo"
                      >
                        Country*
                      </label>

                      <Select
                        options={options}
                        value={country}
                        onChange={changeCountry}
                        placeholder=""
                      />
                      {Object.keys(countryErr).map((key) => {
                        return (
                          <div style={{ color: "red" }}>{countryErr[key]}</div>
                        );
                      })}
                    </div>
                    <div className="form-check" className="check-div">
                      <label className="form-check-label" for="exampleCheck1">
                        <input
                          value={checkBoxValue}
                          onChange={(e) => {
                            debugger;
                            setCheckBoxValue(!checkBoxValue);
                          }}
                          type="checkbox"
                          className="form-check-input ml-0"
                          id="exampleCheck1"
                        />
                        &nbsp;&nbsp;&nbsp;&nbsp; By clicking you are agreeing to
                        our{" "}
                        <span style={{ color: "rgb(179 227 239)" }}>
                          {" "}
                          Privacy Policy
                        </span>{" "}
                        and
                        <span style={{ color: "rgb(179 227 239)" }}>
                          {" "}
                          Terms & Conditions.
                        </span>
                      </label>
                    </div>
                    <button
                      className="buttondemo"
                      type="submit"
                      style={{
                        opacity:
                          (checkBoxValue && isValidField) === false ? 0.2 : 1,
                      }}
                    >
                      BOOK YOUR DEMO NOW
                    </button>
                    {/* <div className="buttondemo" onClick={openModal}>
                      BOOK YOUR DEMO NOW
                    </div> */}
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {demoModalOpen && (
        <ModalComponent
          demoModalOpen={demoModalOpen}
          setDemoModalOpen={setDemoModalOpen}
          setIsValidField={setIsValidField}
          setCheckBoxValue={setCheckBoxValue}
        />
      )}
      <FooterComponent background={footerbgdemo} image="true" />
    </div>
  );
}

export default Demo;
