import React from "react";
import HeaderComponent from "../HeaderComponent/HeaderComponent";
import div1img from "../../assets/api/div1img.png";
import div1title from "../../assets/api/div1title.png";
import FooterComponent from "../FooterComponent/FooterComponent";
import api_div4_img1 from "../../assets/api/api_div4_img1.png";
import div1secondrowimg from "../../assets/api/div1secondrowimg.png";
import api_div4_img2 from "../../assets/api/api_div4_img2.png";
import api_div4_img3 from "../../assets/api/api_div4_img3.png";
import api_div5_img from "../../assets/api/api_div5_img.png";
import api_div6_img1 from "../../assets/api/api_div6_img1.png";
import api_div6_img2 from "../../assets/api/api_div6_img2.png";
import api_div8_img from "../../assets/api/api_div8_img.png";
import api_div3_img from "../../assets/api/api_div3_img.png";
import api_div2_img1 from "../../assets/api/api_div2_img1.png";
import api_div2_img2 from "../../assets/api/api_div2_img2.png";
import api_div2_img3 from "../../assets/api/api_div2_img3.png";
import api_div2_img4 from "../../assets/api/api_div2_img4.png";
import api_div2_img5 from "../../assets/api/api_div2_img5.png";
import api_div2_img6 from "../../assets/api/api_div2_img6.png";
import Footerimg from "../../assets/api/Footerimg.png";

// import api_line1_img from "../../assets/api/api_line1_img.png";
import "./Api.css";

function Api({ activePage }) {
  return (
    <div>
      {/* ======= Hero1 Section ======= */}
      <section id="api_hero1" className="api_hero1 align-items-center">
        <div>
          <HeaderComponent activePage={activePage} />
        </div>
        <div className="container-fluid">
          <div
            className="row justify-content-lg-center api_firstrow1 "
            style={{ marginTop: "1%" }}
          >
            <div
              className="col-lg-6 api_seconddiv1"
              style={{ paddingRight: "10%" }}
            >
              <div className="col-lg-12 api_divOne ">
                <img src={div1img} className="api_div1img" alt="" />
              </div>
              <br />
              <div className="col-lg-12 api_divOne">
                <img src={div1title} className="api_div1title" alt="" />
              </div>
              <div className="col-lg-12 api_divOne ">
                <h5 className="api_text-left api_firstdivtitle1 mb-2">
                  Develop your API test scripts with ease
                </h5>
                <p className="api_text-left api_firstdivcontent1">
                  TestOptimize uses AI to create stable and reliable automated
                  tests faster than ever and to speed-up the executions and
                  maintenance of your automated tests. No coding skills
                  required.
                </p>
              </div>
            </div>
            <div className="col-lg-6 api_thirddiv1">
              <div className="container mb-lg-5 mb-md-5 api_margin_ipad">
                <img
                  src={div1secondrowimg}
                  style={{
                    maxWidth: "90%",
                    marginTop: "20%",
                    marginBottom: "10%",
                  }}
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* End Hero */}
      {/* ======= small div ======= */}
      <div className="container mx-auto col-lg-6 api_smallerdiv ">
        <div className="p-2">Supports most significant technologies</div>
      </div>
      {/* ======= small div ends======= */}

      <div className="container-fluid api_seconddivbg">
        <div className=" api_media_second_div row mt-5 ">
          <div className="col col-lg-4 ">
            <img
              src={api_div2_img1}
              style={{ width: "40%", marginTop: "15%" }}
              alt=""
            />
          </div>
          <div className="col col-lg-4">
            <img
              src={api_div2_img2}
              style={{ width: "40%", marginTop: "25%" }}
              alt=""
            />
          </div>
          <div className="col col-lg-4">
            <img
              src={api_div2_img3}
              style={{ width: "40%", marginTop: "25%" }}
              alt=""
            />
          </div>
        </div>

        <div className="row mt-5 mb-5">
          <div className="col col-lg-4">
            <img
              src={api_div2_img4}
              style={{ width: "40%", marginTop: "5%" }}
              alt=""
            />
          </div>
          <div className="col col-lg-4">
            <img src={api_div2_img5} style={{ width: "40%" }} alt="" />
          </div>
          <div className="col col-lg-4">
            <img src={api_div2_img6} style={{ width: "40%" }} alt="" />
          </div>
        </div>
        {/* ======= small div ======= */}
        <div
          className=" api_media_small container mx-auto col-lg-6 api_smallerdiv mt-5"
          // style={{ marginBottom: "-5%" }}
        >
          <div className="p-2">Inbuilt framework for all your algorithms</div>
        </div>
        {/* ======= small div ends======= */}
        <div className="container">
          <div className="row">
            <div className="col-lg-6 api_media_third_div ">
              <ul className="api_ulList">
                <li className="api_listGroup">
                  Embedded Data driven framework
                </li>
                <li className="api_listGroup">
                  Reuest payload created with parameterized data
                </li>
                <li className="api_listGroup">
                  Parameterized assertions with dynamic data
                </li>
                <li className="api_listGroup">
                  Automatically propagate assertions across repeated sets of
                  response
                </li>

                <li className="api_listGroup">
                  Easily call server APIs from your UI test for validation or
                  data inquiry
                </li>
              </ul>
            </div>
            <div className="col-lg-6 api_media_third_div ">
              <img src={api_div3_img} className="api_divfifthimg" alt="" />
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid api_custombg position-relative">
        <div
          className="api_bar_hide position-absolute"
          style={{
            borderRight: "1px solid #ffffff",
            height: "25%",
            bottom: "0%",
            top: "53%",
            width: "34%",
          }}
        ></div>
        <div
          className="api_bar_hide position-absolute"
          style={{
            borderRight: "1px solid #ffffff",
            height: "25%",
            bottom: "0%",
            top: "53%",
            width: "64%",
          }}
        ></div>
        <div className="row ">
          <div className="col-lg-4 col-md-12 col-sm-12 ">
            <div className="col-lg-12 api_Fourthdivcol1">
              <img src={api_div4_img1} className="api_custom1" alt="" />
            </div>
            <div className="col-lg-12 ">
              <h5 className="api_text-left api_customdatatitle mb-2">
                RESTful Web Service
              </h5>
              <p className="api_text-left api_customdata ">
                JSON RAML Swagger / Open API WADL SOA / Web services/ XML WSDL /
                XML Schema SOAP PoX (Plain XML) /GZIP WS-*Standards
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-12 col-sm-12">
            <div className="col-lg-12 ">
              <img src={api_div4_img2} className="api_custom1" alt="" />
            </div>
            <div className="col-lg-12 ">
              <h5 className="api_text-left api_customdatatitle mb-2">
                Microservices
              </h5>
              <p className="api_text-left api_customdata ">
                Kafka, RabbitMQ, MQTT, AMQP Protobuf WebSockets
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-12 col-sm-12">
            <div className="col-lg-12 mb-4 ">
              <img src={api_div4_img3} className="api_custom1" alt="" />
            </div>
            <div className="col-lg-12 api_Fouthdivcol2">
              <h5 className="api_text-left api_customdatatitle mb-3 ">
                SOAP Web Services
              </h5>
              <p className="api_text-left api_customdata mb-5 ">
                Open API WADL AOA / Web services XML WSDL XML Schema SOAP
                PoX(Plain XML) GZIP WS-*Standards
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="api_seconddivbg">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 col-md-12 col-sm-12 ">
              <img src={api_div5_img} className="api_divimg" alt="" />
            </div>
            <div className="col-lg-6 col-md-12 lg-4 col-sm-12 mb-5">
              <ul className="api_ulList1  api_media_div_four">
                <li className="api_liListTitle">
                  Scriptless API Test Automation, Seamlessly integrated with UI
                  & Database testing
                </li>
                <li className="api_listGroup">Low Code API testing on cloud</li>
                <li className="api_listGroup">
                  API & UI Testing in single Test Script
                </li>
                <li className="api_listGroup">
                  API Test Case Management, Test Planning, Execution and
                  tracking governance
                </li>
                <li className="api_listGroup">Steps Reusability</li>

                <li className="api_listGroup">
                  CI Driven regression suite executions
                </li>
                <li className="api_listGroup">
                  Dynamic Environment management
                </li>
                <li className="api_listGroup">
                  Directly correlate Business process and corresponding API for
                  complete coverage
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid api_custombg position-relative">
        <div
          className="api_bar_hide position-absolute"
          style={{
            borderRight: "2px solid #ffffff",
            height: "90%",
            bottom: "0",
            width: "50%",
          }}
        ></div>
        <div className="row">
          <div className="col-lg-6 mx-auto col-md-12 col-sm-12 ">
            <div className="col-lg-12">
              <img src={api_div6_img1} className="api_custom1" alt="" />
            </div>
            <div className="col-lg-12 ">
              <h5 className="api_text-left api_customdatatitle mb-2">
                Steps Reusability
              </h5>
              <p className="api_text-left api_customdata ">
                With AI deiven now yoy can avoid writing repeated steps which
                come across in all your test cases
              </p>
            </div>
          </div>

          <div className="col-lg-6 mx-auto col-md-12 col-sm-12 mb-4">
            <div className="col-lg-12 api_sixthdivcol2">
              <img src={api_div6_img2} className="api_custom1" alt="" />
            </div>
            <div className="col-lg-12 ">
              <h5 className="api_text-left api_customdatatitle ">
                Custom Functions
              </h5>
              <p className="api_text-left api_customdata ">
                Define your own NLP's which demands your test scenario
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="api_sevendivbg">
        <div className="container">
          <div className="row mt-3 ">
            <div
              className="col-lg-6 col-md-12 col-sm-12 "
              style={{ marginBottom: "-10%" }}
            >
              <img src={api_div8_img} className="api_div8img" alt="" />
            </div>
            <div className="col-lg-6 col-md-12 lg-4 col-sm-12 ">
              <ul className="api_ulList ">
                <li className="api_liListTitlediv7">
                  Execute your API Test Suite faster than ever
                </li>
                <li>
                  Create your API automated test cases quickly with natural
                  language-based test step creation that enables you to
                  formulate test steps using simple English statements.
                </li>
                <li style={{ marginTop: "3%" }}>
                  Test Data Management, and Reusable Steps Groups will help you
                  speed up automating tests.
                </li>
              </ul>
            </div>
          </div>

          <div className="row ">
            <div className="col-lg-6 col-md-12 lg-4 col-sm-12 ">
              <ul className="api_ulList api_media_div7 ">
                <li className="api_liListTitlediv7">
                  Simple Solution to your API regression suite
                </li>
                <li>
                  Create your API automated test cases quickly with natural
                  language-based test step creation that enables you to
                  formulate test steps using simple English statements.
                </li>
                <li style={{ marginTop: "3%" }}>
                  Test Data Management, and Reusable Steps Groups will help you
                  speed up automating tests.
                </li>
              </ul>
            </div>
            <div className="col-lg-6 col-md-12 col-sm-12 mb-5  api_media_div7">
              <img src={api_div8_img} className="api_div8img2" alt="" />
            </div>
          </div>
        </div>
      </div>

      <FooterComponent background={Footerimg} image="true" />
    </div>
  );
}

export default Api;
