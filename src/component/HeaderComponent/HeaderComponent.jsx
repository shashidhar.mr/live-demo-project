import React from "react";
import logo from "../../assets/logo.png";
import "./HeaderComponent.css";
import { Link } from "react-router-dom";

const HeaderComponent = ({ activePage }) => {
  const pricing =
    activePage == "pricing"
      ? `nav-link text-white mx-3  px-0`
      : "nav-link text-white mx-3  px-0 header_a";
  const insights =
    activePage === "insights"
      ? "nav-link text-white mx-3 px-0"
      : "nav-link text-white mx-3 px-0 header_a";
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark  headerComp">
        <Link className="navbar-brand" to="/">
          <img src={logo} alt="" />
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div
          className="collapse navbar-collapse justify-content-end"
          id="navbarNav"
        >
          <ul className="navbar-nav text-left">
            <li className="nav-item active">
              <li className="nav-item">
                <li className="nav-item dropdown">
                  <a
                    className="nav-link text-white mx-3 px-0 header_a"
                    href="#"
                    id="navbarDropdownMenuLink"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Business Solutions
                    <span className="bi bi-chevron-down ml-1"></span>
                  </a>
                  <div
                    className="dropdown-menu dropdown_menu"
                    aria-labelledby="navbarDropdownMenuLink"
                  >
                    <li className="nav-item">
                      <Link
                        className="dropdown-item text-white header_a"
                        to="/web"
                      >
                        Web
                      </Link>
                    </li>
                    <li className="nav-item">
                      <Link
                        className="dropdown-item text-white header_a"
                        to="/api"
                      >
                        API
                      </Link>
                    </li>
                    <li className="nav-item">
                      <Link
                        className="dropdown-item text-white header_a"
                        to="/mobile"
                      >
                        Mobile
                      </Link>
                    </li>
                    <li className="nav-item">
                      <Link className="dropdown-item text-white" to="/database">
                        Database
                      </Link>
                    </li>
                  </div>
                </li>
              </li>
            </li>
            <li className="nav-item ">
              <Link className={pricing} to="/pricing">
                Pricing
              </Link>
            </li>
            <li className="nav-item">
              <Link className={insights} to="/insights">
                Insights
              </Link>
            </li>
            <li className="nav-item">
              <a className="nav-link text-white mx-3 px-0 header_a" href="#">
                Login
              </a>
            </li>
            <li className="nav-item mx-3">
              <div className="d-flex">
                <div className="text-white px-3 py-2 trialBtn">FREE TRAIL</div>
                <div className="px-2 py-2 text-white trialIcon">
                  <span className="bi bi-chevron-right"></span>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </nav>
    </>
  );
};

export default HeaderComponent;
