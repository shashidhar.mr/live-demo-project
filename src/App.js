import "./App.css";
import { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Web from "./component/Web/Web";
import Home from "./component/Home/Home";
import Api from "./component/Api/Api";
import Insights from "./component/Insights/Insights";
import Mobile from "./component/Mobile/Mobile";
import Database from "./component/database/Database";
import ScrollToTop from "react-scroll-to-top";
import { ReactComponent as Arrowup } from "./assets/arrow.svg";
import ModalComponent from "./component/ModalComponent/ModalComponent";
import Demo from "./component/Demo/Demo";
import Pricing from "./component/Pricing/Pricing";

function App() {
  const [activePage, setActivePage] = useState();
  return (
    <div className="App">
      <ScrollToTop component={<Arrowup />} style={{ right: "20px" }} smooth />
      <Router>
        <Switch>
          <Route exact path="/">
            <Home activePage={activePage} />
          </Route>
          <Route exact path="/web">
            <Web activePage={activePage} />
          </Route>
          <Route exact path="/insights">
            <Insights activePage={activePage} setActivePage={setActivePage} />
          </Route>
          <Route exact path="/api">
            <Api activePage={activePage} />
          </Route>
          <Route exact path="/mobile">
            <Mobile activePage={activePage} />
          </Route>
          <Route exact path="/database">
            <Database/>
          </Route>
          {/* <Route exact path="/modalComponent">
            <ModalComponent />
          </Route> */}
          <Route exact path="/requestdemo">
            <Demo />
          </Route>
          <Route exact path="/pricing">
            <Pricing activePage={activePage} setActivePage={setActivePage} />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
